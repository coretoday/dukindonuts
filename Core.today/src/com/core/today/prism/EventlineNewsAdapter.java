package com.core.today.prism;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.example.core.today.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class EventlineNewsAdapter extends BaseAdapter{

	String jsonfile;
	
	public EventlineNewsAdapter(Context context, int resource, String json) {
		super();
		mcontext = context;
		id = resource;
		jsonfile = json;
		
	}
	int id;
	Context mcontext;
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 10;
	}


	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		final int pos = position;
		final Context context = parent.getContext();
		View v = convertView;  

		if(v == null){
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			if(position%4==0)
			v = inflater.inflate(R.layout.eventline_news_item2, parent, false);
			else
				v = inflater.inflate(R.layout.eventline_news_item, parent, false);

			
			TextView textViewTitle = (TextView) v.findViewById(R.id.eventline_item_title);
	        ImageView newsimage = (ImageView) v.findViewById(R.id.eventline_item_image);
			
	        JSONObject test = null;
	        
	       
	        try {
	        	   
				JSONArray alljson = new JSONArray(jsonfile);
				test = new JSONObject(alljson.getString(position));
				URL articleURL = null;
				articleURL = new URL(test.getString("i"));
				
				HttpURLConnection conn = (HttpURLConnection)articleURL.openConnection();   
	            conn.setDoInput(true);   
	            conn.connect();   
				InputStream is = conn.getInputStream(); 
				Bitmap bmImg; 
				bmImg = BitmapFactory.decodeStream(is);
	           
	        	bmImg = resizeBitmapImageFn(bmImg);
	        	conn.disconnect();

				textViewTitle.setText(test.getString("k"));
				newsimage.setImageBitmap(bmImg);
	           } 
	           catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
	           }
	      
		}
		return v;
	}
	
	public Bitmap resizeBitmapImageFn(Bitmap bmpSource){    		        
    		        
		return Bitmap.createScaledBitmap(bmpSource, 50, 50, true); 
	}


	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

}
