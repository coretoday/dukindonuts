package com.core.today.prism;

import com.example.core.today.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.MenuItem.OnMenuItemClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class FullNews extends Activity{

	private GridView                mGridView;
	private ArrayAdapter<Bitmap>    mAdapterPic;
    private FullNewsAdapter    mAdapterText;
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_news);
		////////////////////////////////////////////////////////////////////////////////
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#e15d52")));
		getActionBar().setHomeButtonEnabled(true);
		int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		TextView actionBarTitleView = (TextView)getWindow().findViewById(actionBarTitle);
		Typeface font1 = Typeface.createFromAsset(getAssets(), "Pacifico.ttf");
		actionBarTitleView.setTypeface(font1);
		getActionBar().setTitle("Prism");
		getActionBar().setDisplayOptions(actionBar.DISPLAY_HOME_AS_UP | actionBar.DISPLAY_SHOW_HOME | actionBar.DISPLAY_SHOW_TITLE);
		////////////////////////////////////////////////////////////////////////////////
      
        mAdapterText = new FullNewsAdapter();
         
      
        mGridView = (GridView) findViewById(R.id.gridView1);
         
       
        mGridView.setAdapter(mAdapterText);
         
      
        mGridView.setOnItemClickListener(onClickListItem);
         
        
    }
    
    private OnItemClickListener onClickListItem = new OnItemClickListener() {
    	 
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            
            
        }

		
    };
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(android.R.id.home == item.getItemId()){
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}
