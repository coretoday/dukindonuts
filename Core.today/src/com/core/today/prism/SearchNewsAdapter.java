package com.core.today.prism;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.example.core.today.R;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

public class SearchNewsAdapter extends BaseAdapter implements
		StickyListHeadersAdapter, SectionIndexer{

	String jsonfile;
    private String[] mCountries;
	private int[] mSectionIndices;
    private Character[] mSectionDates;
    private LayoutInflater mInflater;
	
	public SearchNewsAdapter(Context context, int resource, String json) {
		
	    mInflater = LayoutInflater.from(context);
		mcontext = context;
		id = resource;
		jsonfile = json;
        mCountries = context.getResources().getStringArray(R.array.countries);
        mSectionIndices = getSectionIndices();
	    mSectionDates = getSectionDates();
	}
	int id;
	Context mcontext;
	
	private int[] getSectionIndices() {//구간 어떻게 나눌 것인지 section 구분하는 기
        ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
        char lastFirstChar = mCountries[0].charAt(0);
        sectionIndices.add(0);
        for (int i = 1; i < mCountries.length; i++) {
            if (mCountries[i].charAt(0) != lastFirstChar) {
                lastFirstChar = mCountries[i].charAt(0);
                sectionIndices.add(i);
            }
        }
        int[] sections = new int[sectionIndices.size()];
        for (int i = 0; i < sectionIndices.size(); i++) {
            sections[i] = sectionIndices.get(i);
        }
        return sections;
		
	}
	
	private Character[] getSectionDates() {// section 구분선에 해당하는 날짜가 들어갈 값.(별로안중요)
		   Character[] letters = new Character[mSectionIndices.length];
	        for (int i = 0; i < mSectionIndices.length; i++) {
	            letters[i] = mCountries[mSectionIndices[i]].charAt(0);
	        }
	        return letters;
    }
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		 return mCountries.length;
	}


	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		 return mCountries[position];
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		View v = convertView;
		
		if(v == null){
			
			holder = new ViewHolder();
			
			v = mInflater.inflate(R.layout.search_news_item, parent, false);

			holder.title = (TextView) v.findViewById(R.id.search_item_title);
	        holder.image = (ImageView) v.findViewById(R.id.search_item_image);
			
	        /*JSONObject test = null;
	        try {
	        	   
				JSONArray alljson = new JSONArray(jsonfile);
				test = new JSONObject(alljson.getString(position));
				URL articleURL = null;
				articleURL = new URL(test.getString("i"));
				
				HttpURLConnection conn = (HttpURLConnection)articleURL.openConnection();   
	            conn.setDoInput(true);   
	            conn.connect();   
				InputStream is = conn.getInputStream(); 
				Bitmap bmImg; 
				bmImg = BitmapFactory.decodeStream(is);
	           
	        	bmImg = resizeBitmapImageFn(bmImg);
	        	conn.disconnect();
	        	
	           } 
	           catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
	           }
	      */
           v.setTag(holder);

		}
		else {
	            holder = (ViewHolder) v.getTag();
	            
	    }
		
		holder.title.setText(mCountries[position]);
		//holder.image.setImageBitmap(bmImg);
		
		return v;
	}
	
	@Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;

        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = mInflater.inflate(R.layout.header, parent, false);
            holder.date = (TextView) convertView.findViewById(R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        
        CharSequence headerChar = mCountries[position].subSequence(0, 1);
        holder.date.setText(headerChar);
        // set header text as first char in name
        // 이작업 해놓을것 setText부분.
        //CharSequence headerChar = mCountries[position].subSequence(0, 1);
        //holder.date.setText(headerChar);

        return convertView;
    }
	
	/**
     * Remember that these have to be static, postion=1 should always return
     * the same Id that is.
     */
    @Override
    public long getHeaderId(int position) {
        // return the first character of the country as ID because this is what
        // headers are based upon
        //return mCountries[position].subSequence(0, 1).charAt(0);
        return mCountries[position].subSequence(0, 1).charAt(0);

    }

    @Override
    public int getPositionForSection(int section) {
        if (mSectionIndices.length == 0) {
            return 0;
        }
        
        if (section >= mSectionIndices.length) {
            section = mSectionIndices.length - 1;
        } else if (section < 0) {
            section = 0;
        }
        return mSectionIndices[section];
    }

    @Override
    public int getSectionForPosition(int position) {
        for (int i = 0; i < mSectionIndices.length; i++) {
            if (position < mSectionIndices[i]) {
                return i - 1;
            }
        }
        return mSectionIndices.length - 1;
    }

    @Override
    public Object[] getSections() {
        return mSectionDates;
    }
    

	class HeaderViewHolder {
        TextView date;
    }

    class ViewHolder {
        TextView title;
        ImageView image;
    }
    
    public Bitmap resizeBitmapImageFn(Bitmap bmpSource){    		        
        
		return Bitmap.createScaledBitmap(bmpSource, 50, 50, true); 
	}


}
