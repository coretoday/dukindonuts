package com.core.today.prism;


public class JsonNewsItem2 {
	private String keyword;
	private String image;
	private String URL;

	public JsonNewsItem2(String keyword, String image, String URL) {
		this.keyword = keyword;
		this.image = image;
		this.URL = URL;
	}

	public String getTitle() {
		return keyword;
	}

	public void setTitle(String title) {
		this.keyword = title;
	}
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getURL() {
		return URL;
	}
	public void setURL(String URL) {
		this.URL=URL;
	}

}
