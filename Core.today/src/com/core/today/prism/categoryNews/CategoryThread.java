package com.core.today.prism.categoryNews;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.core.today.prism.JsonNewsItem2;

import android.content.Context;
import android.widget.GridView;
import android.widget.Toast;

public class CategoryThread implements Runnable{

	private String category;
	private CategoryAdapter gridViewCustomAdapter;
	private Context mcontext;
	private ArrayList<JsonNewsItem2> arraylistOfNews;
	private int get_id;
	public GridView gridView; 
	CategoryThread(String category,int id, Context cont, GridView gridView){
		
		this.category = category;
		mcontext = cont;
		get_id = id;
		this.gridView = gridView;
		
	}
	
	
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			CategoryMaker();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			Toast.makeText(mcontext, "������ ���� �����忡�� ������", Toast.LENGTH_LONG).show();
		}
		
		
	}

	private void CategoryMaker() throws UnsupportedEncodingException{
		String line = null;
		String page ="";
		String group1 = null;

		arraylistOfNews = new ArrayList<JsonNewsItem2>();
		group1 = "http://kr.core.today/json2/?n=50&sl=1&s=-1&c="+URLEncoder.encode(category, "UTF-8");

		String sample = group1;

		URL url;
		HttpURLConnection urlConnection;
		BufferedReader bufreader;
		try {
			url = new URL(sample);
			urlConnection = (HttpURLConnection) url.openConnection();
			bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
			while((line=bufreader.readLine())!=null){
				page+=line;
			}
			urlConnection.disconnect();
		}
		catch(Exception e){
			Toast.makeText(mcontext, "������ ���� url�ҷ����°ſ��� ������", Toast.LENGTH_LONG).show();
		}
		JSONObject test = null ;         

		JSONArray alljson = null;
		try {
			alljson = new JSONArray(page);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();

			Toast.makeText(mcontext, "������ ���� page�޴°ſ��� ������!", Toast.LENGTH_LONG).show();

		}
		String keyword;
		String image;
		for(int k =0; k < 10 ; k++){

			try{
				test=new JSONObject(alljson.getString(k));
			}
			catch(Exception e){
				Toast.makeText(mcontext, "������ ���� �Ľ̿��� ������1", Toast.LENGTH_SHORT).show();
			}
			try{
				JSONArray keyw = new JSONArray(test.getString("a"));
				//    if(keyw.getString(0) != "[]" && test.getString("nm") != "[]"){

				//JSONArray arti = new JSONArray(test.getString("nm"));
				keyword=test.getString("k");
				JSONObject URLParser = new JSONObject(keyw.getString(0));

				image = test.getString("i");
				arraylistOfNews.add(new JsonNewsItem2(keyword, image, URLParser.getString("url")));
				
			}


			catch(Exception e){
				Toast.makeText(mcontext, "������ ���� �Ľ̿��� ������2", Toast.LENGTH_SHORT).show();
			}
		}
		gridViewCustomAdapter = new CategoryAdapter(mcontext, get_id, arraylistOfNews);
		gridView.setAdapter(gridViewCustomAdapter);
	}
	public ArrayList<JsonNewsItem2> getItems(){
		return arraylistOfNews;
	}
}
