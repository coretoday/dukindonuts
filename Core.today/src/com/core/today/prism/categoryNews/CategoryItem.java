package com.core.today.prism.categoryNews;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.core.today.prism.JsonNewsItem2;
import com.example.core.today.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;


public class CategoryItem extends Activity{

	String pager = null;
	CategoryAdapter gridViewCustomAdapter;
	private String category;
	ArrayList<JsonNewsItem2> arraylistOfNews;
	GridView gridView;
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		setContentView(R.layout.category_news);
		
		////////////////////////////////////////////////////////////////////////////////
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#e15d52")));
		getActionBar().setHomeButtonEnabled(true);
		int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		TextView actionBarTitleView = (TextView)getWindow().findViewById(actionBarTitle);
		Typeface font1 = Typeface.createFromAsset(getAssets(), "THEjunggt120.otf");
		actionBarTitleView.setTypeface(font1);
		getActionBar().setTitle("PRISM");
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
		////////////////////////////////////////////////////////////////////////////////
		Intent receivedintent = getIntent();
		int get_id = receivedintent.getExtras().getInt("category");
		if(get_id == R.id.ctg_economy)
			category = "����";
		else if(get_id == R.id.ctg_entertain)
			category = "����";
		else if(get_id == R.id.ctg_global)
			category = "�۷ι�";
		else if(get_id == R.id.ctg_it)
			category = "IT";
		else if(get_id == R.id.ctg_main)
			category = "�ֿ䴺��";
		else if(get_id == R.id.ctg_politics)
			category = "��ġ��ȸ";
		else if(get_id == R.id.ctg_sports)
			category = "������";
		


		TextView text = (TextView)findViewById(R.id.ctg);
		text.setText(category);
/*
		try {
			CategoryMaker();
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			Toast.makeText(getApplicationContext(), e1.toString(), Toast.LENGTH_LONG).show();
		}
		//Toast.makeText(getApplication(), "hihihiihihi", Toast.LENGTH_LONG).show();
		gridViewCustomAdapter = new CategoryAdapter(this, get_id, arraylistOfNews);
		gridViewCustomAdapter.getCount(10);
		gridView.setAdapter(gridViewCustomAdapter);
*/
		gridView = (GridView) findViewById(R.id.gridView);
		final Runnable parser = new CategoryThread(category, get_id, getApplication(), gridView);
		Thread parsing = new Thread(parser);
		parsing.start();
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				arraylistOfNews = ((CategoryThread) parser).getItems();
				Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(arraylistOfNews.get(position).getURL()));
				startActivity(intent);
			}
		});
	}
	/*
	private void CategoryMaker() throws UnsupportedEncodingException{
		String line = null;
		String page ="";
		String group1 = null;

		arraylistOfNews = new ArrayList<JsonNewsItem2>();
		group1 = "http://kr.core.today/json2/?n=50&sl=1&s=-1&c="+URLEncoder.encode(category, "UTF-8");

		String sample = group1;

		URL url;
		HttpURLConnection urlConnection;
		BufferedReader bufreader;
		try {
			url = new URL(sample);
			urlConnection = (HttpURLConnection) url.openConnection();
			bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
			while((line=bufreader.readLine())!=null){
				page+=line;
			}
			urlConnection.disconnect();
		}
		catch(Exception e){
			Toast.makeText(getApplication(), "������ ���� url�ҷ����°ſ��� ������", Toast.LENGTH_LONG).show();
		}
		JSONObject test = null ;         

		JSONArray alljson = null;
		try {
			alljson = new JSONArray(page);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();

			Toast.makeText(getApplication(), "������ ���� page�޴°ſ��� ������!", Toast.LENGTH_LONG).show();

		}

		String keyword;
		String image;
		for(int k =0; k < 10 ; k++){

			try{
				test=new JSONObject(alljson.getString(k));
			}
			catch(Exception e){
				Toast.makeText(getApplication(), "������ ���� �Ľ̿��� ������1", 3000).show();
			}
			try{
				JSONArray keyw = new JSONArray(test.getString("a"));
				//    if(keyw.getString(0) != "[]" && test.getString("nm") != "[]"){

				//JSONArray arti = new JSONArray(test.getString("nm"));
				keyword=test.getString("k");
				JSONObject URLParser = new JSONObject(keyw.getString(0));

				image = test.getString("i");
				arraylistOfNews.add(new JsonNewsItem2(keyword, image, URLParser.getString("url")));
			}


			catch(Exception e){
				Toast.makeText(getApplication(), "������ ���� �Ľ̿��� ������2", 3000).show();
			}
		}    
	}
	*/
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(android.R.id.home == item.getItemId()){
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}