package com.core.today.prism.categoryNews;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import com.core.today.prism.JsonNewsItem2;
import com.example.core.today.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressWarnings("rawtypes")
public class CategoryAdapter extends ArrayAdapter{

	ArrayList<JsonNewsItem2> jsonfile;
	public int count =0;
	int MaxNewsItems =10;
	String keywords;
	String ImgURL;


	public CategoryAdapter(Context context, int resource, ArrayList<JsonNewsItem2> json) {
		super(context, resource);
		mcontext = context;
		id = resource;
		jsonfile = json;
	}
	int id;
	Context mcontext;


	public void getCount(int numOfItems){
		MaxNewsItems = numOfItems;
	}

	public int getCount(){
		return 10;
	}
	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View row = convertView;
		LayoutInflater inflater = ((Activity) mcontext).getLayoutInflater();
		row = inflater.inflate(R.layout.grid_row, parent, false);                        
		TextView textViewTitle = (TextView) row.findViewById(R.id.title_text);
		ImageView newsimage = (ImageView) row.findViewById(R.id.image);

        try{
           
        	keywords = jsonfile.get(position).getTitle();
        	ImgURL = jsonfile.get(position).getImage();
        
        }
        catch(Exception e){
        	Toast.makeText(getContext(), "������ gridVIew ����°� ������", 2000).show();
        	e.printStackTrace();      
        }          
        if(ImgURL != "null"){
        
        	try{
        		URL articleURL = null;
        		articleURL = new URL(ImgURL);
        		HttpURLConnection conn = (HttpURLConnection)articleURL.openConnection();   
        		conn.setDoInput(true);   
        		conn.connect();   
        		InputStream is = conn.getInputStream(); 
        		Bitmap bmImg ;
        		bmImg = BitmapFactory.decodeStream(is);
        		conn.disconnect();
        		newsimage.setImageBitmap(bmImg);	
        		
        	}
        	catch(Exception e){
        		e.printStackTrace();
        	}        
        }
        textViewTitle.setText(keywords);                          
        return row;
	}
}




